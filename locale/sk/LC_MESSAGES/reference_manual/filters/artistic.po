# translation of docs_krita_org_reference_manual___filters___artistic.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___artistic\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 10:25+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters/artistic.rst:1
msgid "Overview of the artistic filters."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:11
#: ../../reference_manual/filters/artistic.rst:21
msgid "Halftone"
msgstr "Polotónovanie"

#: ../../reference_manual/filters/artistic.rst:11
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/artistic.rst:11
msgid "HD Index Painting"
msgstr ""

#: ../../reference_manual/filters/artistic.rst:16
msgid "Artistic"
msgstr "Artistic"

#: ../../reference_manual/filters/artistic.rst:18
msgid ""
"The artistic filter are characterised by taking an input, and doing a "
"deformation on them."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:24
#, fuzzy
#| msgid ".. image:: images/en/Krita_halftone_filter.png"
msgid ".. image:: images/filters/Krita_halftone_filter.png"
msgstr ".. image:: images/en/Krita_halftone_filter.png"

#: ../../reference_manual/filters/artistic.rst:25
msgid ""
"The halftone filter is a filter that converts the colors to a halftone dot "
"pattern."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:27
msgid "Colors"
msgstr "Farby"

#: ../../reference_manual/filters/artistic.rst:28
msgid ""
"The colors used to paint the pattern. The first is the color of the dots, "
"the second the color of the background."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:29
msgid "Size"
msgstr "Veľkosť"

#: ../../reference_manual/filters/artistic.rst:30
msgid ""
"The size of the cell in pixels. The maximum dot size will be using the "
"diagonal as the cell size to make sure you can have pure black."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:31
msgid "Angle"
msgstr "Uhol"

#: ../../reference_manual/filters/artistic.rst:32
msgid "The angle of the dot pattern."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:33
msgid "Invert"
msgstr "Invertovať"

#: ../../reference_manual/filters/artistic.rst:34
msgid ""
"This inverts the intensity calculated per dot. Thus, dark colors will give "
"tiny dots, and light colors big dots. This is useful in combination with "
"inverting the colors, and give a better pattern on glowy-effects."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:36
msgid "Anti-aliasing"
msgstr "Vyhladzovanie"

#: ../../reference_manual/filters/artistic.rst:36
msgid ""
"This makes the dots smooth, which is good for webgraphics. Sometimes, for "
"print graphics, we want there to be no grays, so we turn off the anti-"
"aliasing."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:39
msgid "Index Color"
msgstr ""

#: ../../reference_manual/filters/artistic.rst:41
msgid ""
"The index color filter maps specific user selected colors to the grayscale "
"value of the artwork. You can see the example below, the strip below the "
"black and white gradient has index color applied to it so that the black and "
"white gradient gets the color selected to different values."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:44
#, fuzzy
#| msgid ".. image:: images/en/Gradient-pixelart.png"
msgid ".. image:: images/common-workflows/Gradient-pixelart.png"
msgstr ".. image:: images/en/Gradient-pixelart.png"

#: ../../reference_manual/filters/artistic.rst:45
msgid ""
"You can choose the required colors and ramps in the index color filter "
"dialog as shown below ."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:48
#, fuzzy
#| msgid ".. image:: images/en/Index-color-filter.png"
msgid ".. image:: images/filters/Index-color-filter.png"
msgstr ".. image:: images/en/Index-color-filter.png"

#: ../../reference_manual/filters/artistic.rst:49
msgid ""
"You can create index painting such as one shown below with the help of this "
"filter."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:52
#, fuzzy
#| msgid ".. image:: images/en/Kiki-pixel-art.png"
msgid ".. image:: images/common-workflows/Kiki-pixel-art.png"
msgstr ".. image:: images/en/Kiki-pixel-art.png"

#: ../../reference_manual/filters/artistic.rst:54
msgid "Pixelize"
msgstr "Pixelizovať"

#: ../../reference_manual/filters/artistic.rst:56
msgid ""
"Makes the input-image pixely by creating small cells and inputting an "
"average color."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:59
#, fuzzy
#| msgid ".. image:: images/en/Pixelize-filter.png"
msgid ".. image:: images/filters/Pixelize-filter.png"
msgstr ".. image:: images/en/Pixelize-filter.png"

#: ../../reference_manual/filters/artistic.rst:61
msgid "Raindrops"
msgstr "Dažďové kvapky"

#: ../../reference_manual/filters/artistic.rst:63
msgid "Adds random raindrop-deformations to the input-image."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:66
msgid "Oilpaint"
msgstr "Olejomaľba"

#: ../../reference_manual/filters/artistic.rst:68
msgid ""
"Does semi-posterisation to the input-image, with the 'brush-size' "
"determining the size of the fields."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:71
#, fuzzy
#| msgid ".. image:: images/en/Oilpaint-filter.png"
msgid ".. image:: images/filters/Oilpaint-filter.png"
msgstr ".. image:: images/en/Oilpaint-filter.png"

#: ../../reference_manual/filters/artistic.rst:72
msgid "Brush-size"
msgstr "Veľkosť štetca"

#: ../../reference_manual/filters/artistic.rst:73
msgid ""
"Determines how large the individual patches are. The lower, the more "
"detailed."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:75
msgid "Smoothness"
msgstr "Hladkosť"

#: ../../reference_manual/filters/artistic.rst:75
msgid "Determines how much each patch's outline is smoothed out."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:78
msgid "Posterize"
msgstr "Posterizovať"

#: ../../reference_manual/filters/artistic.rst:80
msgid ""
"This filter decreases the amount of colors in an image. It does this per "
"component (channel)."
msgstr ""

#: ../../reference_manual/filters/artistic.rst:83
#, fuzzy
#| msgid ".. image:: images/en/Posterize-filter.png"
msgid ".. image:: images/filters/Posterize-filter.png"
msgstr ".. image:: images/en/Posterize-filter.png"

#: ../../reference_manual/filters/artistic.rst:84
msgid ""
"The :guilabel:`Steps` parameter determines how many colors are allowed per "
"component."
msgstr ""
