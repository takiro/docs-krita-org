# Translation of docs_krita_org_reference_manual___preferences___canvas_only_mode.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:12+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/preferences/canvas_only_mode.rst:1
msgid "Canvas only mode settings in Krita."
msgstr "Ajustaments per al mode només el llenç al Krita."

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
#: ../../reference_manual/preferences/canvas_only_mode.rst:16
msgid "Canvas Only Mode"
msgstr "Mode només el llenç"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/canvas_only_mode.rst:18
msgid ""
"Canvas Only mode is Krita's version of full screen mode. It is activated by "
"hitting the :kbd:`Tab` key on the keyboard. Select which parts of Krita will "
"be hidden in canvas-only mode -- The user can set which UI items will be "
"hidden in canvas-only mode. Selected items will be hidden."
msgstr ""
"El mode Només el llenç és la versió del Krita de la pantalla completa. "
"S'activa prement la tecla :kbd:`Tab`. Seleccioneu quines parts del Krita "
"s'ocultaran en el mode Només llenç -l'usuari podrà establir quins elements "
"de la IU s'ocultaran en el mode Només el llenç-. Els elements seleccionats "
"seran ocultats."
