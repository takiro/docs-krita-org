# Translation of docs_krita_org_reference_manual___filters___adjust.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-17 16:54+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Power"
msgstr "Potència"

#: ../../reference_manual/filters/adjust.rst:None
msgid ".. image:: images/filters/Krita_filters_asc_cdl.png"
msgstr ".. image:: images/filters/Krita_filters_asc_cdl.png"

#: ../../reference_manual/filters/adjust.rst:1
msgid "Overview of the adjust filters."
msgstr "Vista general dels filtres per ajustar."

#: ../../reference_manual/filters/adjust.rst:11
msgid "Filters"
msgstr "Filtres"

#: ../../reference_manual/filters/adjust.rst:16
msgid "Adjust"
msgstr "Ajusta"

#: ../../reference_manual/filters/adjust.rst:18
msgid ""
"The Adjustment filters are image-wide and are for manipulating colors and "
"contrast."
msgstr ""
"Els filtres d'ajust són per a tota la imatge i serveixen per a manipular els "
"colors i el contrast."

#: ../../reference_manual/filters/adjust.rst:20
#: ../../reference_manual/filters/adjust.rst:23
msgid "Dodge"
msgstr "Esvaeix"

#: ../../reference_manual/filters/adjust.rst:25
msgid ""
"An image-wide dodge-filter. Dodge is named after a trick in traditional dark-"
"room photography that gave the same results."
msgstr ""
"Un filtre per esvair a tota la imatge. Esvaeix porta el nom d'un truc de la "
"fotografia tradicional a la cambra fosca que donava els mateixos resultats."

#: ../../reference_manual/filters/adjust.rst:28
msgid ".. image:: images/filters/Dodge-filter.png"
msgstr ".. image:: images/filters/Dodge-filter.png"

#: ../../reference_manual/filters/adjust.rst:29
#: ../../reference_manual/filters/adjust.rst:47
msgid "Shadows"
msgstr "Ombres"

#: ../../reference_manual/filters/adjust.rst:30
#: ../../reference_manual/filters/adjust.rst:48
msgid "The effect will mostly apply to dark tones."
msgstr "L'efecte s'aplicarà principalment als tons foscos."

#: ../../reference_manual/filters/adjust.rst:31
#: ../../reference_manual/filters/adjust.rst:49
msgid "Midtones"
msgstr "Tons mitjans"

#: ../../reference_manual/filters/adjust.rst:32
#: ../../reference_manual/filters/adjust.rst:50
msgid "The effect will apply to mostly midtones."
msgstr "L'efecte s'aplicarà principalment als tons mitjans."

#: ../../reference_manual/filters/adjust.rst:33
#: ../../reference_manual/filters/adjust.rst:51
msgid "Highlights"
msgstr "Llums intenses"

#: ../../reference_manual/filters/adjust.rst:34
#: ../../reference_manual/filters/adjust.rst:52
msgid "This will apply the effect on the highlights only."
msgstr "Només aplicarà l'efecte a les llums intenses."

#: ../../reference_manual/filters/adjust.rst:36
#: ../../reference_manual/filters/adjust.rst:54
msgid "Exposure"
msgstr "Exposició"

#: ../../reference_manual/filters/adjust.rst:36
#: ../../reference_manual/filters/adjust.rst:54
msgid "The strength at which this filter is applied."
msgstr "La intensitat amb la qual s'aplicarà aquest filtre."

#: ../../reference_manual/filters/adjust.rst:38
#: ../../reference_manual/filters/adjust.rst:41
msgid "Burn"
msgstr "Cremat"

#: ../../reference_manual/filters/adjust.rst:43
msgid ""
"An image-wide burn-filter. Burn is named after a trick in traditional dark-"
"room photography that gave similar results."
msgstr ""
"Un filtre per cremar a tota la imatge. Cremat porta el nom d'un truc de la "
"fotografia tradicional a la cambra fosca que donava resultats similars."

#: ../../reference_manual/filters/adjust.rst:46
msgid ".. image:: images/filters/Burn-filter.png"
msgstr ".. image:: images/filters/Burn-filter.png"

#: ../../reference_manual/filters/adjust.rst:56
msgid "Levels Filter"
msgstr "Filtre de nivells"

#: ../../reference_manual/filters/adjust.rst:59
msgid "Levels"
msgstr "Nivells"

#: ../../reference_manual/filters/adjust.rst:61
msgid ""
"This filter allows you to directly modify the levels of the tone-values of "
"an image, by manipulating sliders for highlights, midtones and shadows. You "
"can even set an output and input range of tones for the image. A histogram "
"is displayed to show you the tonal distribution. The default shortcut for "
"levels filter is :kbd:`Ctrl + L`."
msgstr ""
"Aquest filtre permet modificar directament els nivells dels valors de to "
"d'una imatge, manipulant els controls lliscants per a les llums intenses, "
"tons mitjans i ombres. Fins i tot podreu establir un interval de la sortida "
"i entrada de tons per a la imatge. Es mostrarà un histograma per a mostrar-"
"vos la distribució tonal. La drecera predeterminada per al filtre de nivells "
"és :kbd:`Ctrl + L`."

#: ../../reference_manual/filters/adjust.rst:65
msgid ".. image:: images/filters/Levels-filter.png"
msgstr ".. image:: images/filters/Levels-filter.png"

#: ../../reference_manual/filters/adjust.rst:66
msgid ""
"This is very useful to do an initial cleanup of scanned lineart or grayscale "
"images. If the scanned lineart is light you can slide the black triangle to "
"right to make it darker or if you want to remove the gray areas you can "
"slide the white slider to left."
msgstr ""
"És molt útil per a fer una neteja inicial de les imatges escanejades amb "
"línia artística o en escala de grisos. Si la línia artística escanejada és "
"clara, podreu lliscar pel triangle negre cap a la dreta per a fer-la més "
"fosca o si voleu eliminar les àrees grises, podreu fer lliscar el control "
"lliscant blanc cap a l'esquerra."

#: ../../reference_manual/filters/adjust.rst:68
msgid ""
"Auto levels is a quick way to adjust tone of an image. If you want to change "
"the settings later you can click on the :guilabel:`Create Filter Mask` "
"button to add the levels as a filter mask."
msgstr ""
"Els nivells automàtics són una manera ràpida d'ajustar el to d'una imatge. "
"Si voleu canviar els ajustaments més endavant, podreu fer clic al botó :"
"guilabel:`Crea una màscara de filtratge` per afegir els nivells com una "
"màscara de filtratge."

#: ../../reference_manual/filters/adjust.rst:71
#: ../../reference_manual/filters/adjust.rst:74
msgid "Color Adjustment Curves"
msgstr "Corbes d'ajust del color"

#: ../../reference_manual/filters/adjust.rst:71
msgid "RGB Curves"
msgstr "Corbes RGB"

#: ../../reference_manual/filters/adjust.rst:71
msgid "Curves Filter"
msgstr "Corbes del filtre"

#: ../../reference_manual/filters/adjust.rst:76
msgid ""
"This filter allows you to adjust each channel by manipulating the curves. "
"You can even adjust the alpha channel and the lightness channel through this "
"filter. This is used very often by artists as a post processing filter to "
"slightly heighten the mood of the painting by adjust the overall color. For "
"example a scene with fire breathing dragon may be made more red and yellow "
"by adjusting the curves to give it more warmer look, similarly a snowy "
"mountain scene can be made to look cooler by adjusting the blues and greens. "
"The default shortcut for this filter is :kbd:`Ctrl + M`."
msgstr ""
"Aquest filtre permet ajustar cada canal manipulant les corbes. Fins i tot "
"podreu ajustar el canal alfa i el canal de claredat a través d'aquest "
"filtre. És utilitzat molt sovint pels artistes com un filtre de "
"postprocessament per a realçar lleugerament l'ambient de la pintura ajustant "
"el color general. Per exemple, una escena amb un drac que expira foc pot fer-"
"se més vermella i groga ajustant les corbes per donar-li un aspecte més "
"càlid, de manera similar, es pot fer que una escena de muntanya nevada es "
"vegi més fresca ajustant els blaus i verds. La drecera predeterminada per a "
"aquest filtre és :kbd:`Ctrl + M`."

#: ../../reference_manual/filters/adjust.rst:81
msgid "Since 4.1 this filter can also handle Hue and Saturation curves."
msgstr "Aquest filtre també pot gestionar les corbes del To i Saturació."

#: ../../reference_manual/filters/adjust.rst:84
msgid ".. image:: images/filters/Color-adjustment-curve.png"
msgstr ".. image:: images/filters/Color-adjustment-curve.png"

#: ../../reference_manual/filters/adjust.rst:85
msgid "Cross Channel Color Adjustment"
msgstr "Ajust del color entre canals"

#: ../../reference_manual/filters/adjust.rst:85
msgid "Driving Adjustment by channel"
msgstr "Portar l'ajust per canal"

#: ../../reference_manual/filters/adjust.rst:88
msgid "Cross-channel color adjustment"
msgstr "Ajust del color entre canals"

#: ../../reference_manual/filters/adjust.rst:92
msgid ""
"Sometimes, when you are adjusting the colors for an image, you want bright "
"colors to be more saturated, or have a little bit of brightness in the "
"purples."
msgstr ""
"A vegades, quan esteu ajustant els colors d'una imatge, voldreu que els "
"colors brillants estiguin més saturats, o que tinguin una mica de brillantor "
"en els porpres."

#: ../../reference_manual/filters/adjust.rst:94
msgid "The Cross-channel color adjustment filter allows you to do this."
msgstr "El filtre d'ajust del color entre canals permet fer-ho."

#: ../../reference_manual/filters/adjust.rst:96
msgid ""
"At the top, there are two drop-downs. The first one is to choose which :"
"guilabel:`Channel` you wish to modify. The :guilabel:`Driver Channel` drop "
"down is what channel you use to control which parts are modified."
msgstr ""
"A la part superior, hi ha dos desplegables. El primer és per triar quin :"
"guilabel:`Canal` voleu modificar. El desplegable :guilabel:`Controlador del "
"canal` és el canal que s'utilitza per a controlar quines parts es "
"modificaran."

#: ../../reference_manual/filters/adjust.rst:99
msgid ".. image:: images/filters/cross_channel_filter.png"
msgstr ".. image:: images/filters/cross_channel_filter.png"

#: ../../reference_manual/filters/adjust.rst:100
msgid ""
"The curve, on the horizontal axis, represents the driver channel, while the "
"vertical axis represent the channel you wish to modify."
msgstr ""
"La corba, en l'eix horitzontal, representa el controlador del canal, mentre "
"que l'eix vertical representa el canal que voleu modificar."

#: ../../reference_manual/filters/adjust.rst:102
msgid ""
"So if you wish to increase the saturation in the lighter parts, you pick :"
"guilabel:`Saturation` in the first drop-down, and :guilabel:`Lightness` as "
"the driver channel. Then, pull up the right end to the top."
msgstr ""
"Per tant, si voleu fer augmentar la saturació en les parts més clares, trieu "
"la :guilabel:`Saturació` en el primer desplegable i la :guilabel:`Claredat` "
"com el controlador del canal. Després, estireu l'extrem dret cap a la part "
"superior."

#: ../../reference_manual/filters/adjust.rst:104
msgid ""
"If you wish to desaturate everything but the teal/blues, you select :"
"guilabel:`Saturation` for the channel and :guilabel:`Hue` for the driver. "
"Then put a dot in the middle and pull down the dots on either sides."
msgstr ""
"Si voleu reduir la saturació de tot menys el verd blavós i els blaus, "
"seleccioneu :guilabel:`Saturació` per al canal i :guilabel:`To` per al "
"controlador. Després, col·loqueu un punt al mig i estireu dels punts en "
"ambdós costats."

#: ../../reference_manual/filters/adjust.rst:107
msgid "Brightness/Contrast curves"
msgstr "Corbes de la brillantor i contrast"

#: ../../reference_manual/filters/adjust.rst:109
msgid ""
"This filter allows you to adjust the brightness and contrast of the image by "
"adjusting the curves."
msgstr ""
"Aquest filtre permet ajustar la brillantor i el contrast de la imatge "
"ajustant les corbes."

#: ../../reference_manual/filters/adjust.rst:113
msgid ""
"These have been removed in Krita 4.0, because the Color Adjustment filter "
"can do the same. Old files with brightness/contrast curves will be loaded as "
"Color Adjustment curves."
msgstr ""
"Ha estat eliminat en el Krita 4.0, perquè el filtre d'Ajust del color pot "
"fer el mateix. Els fitxers antics amb corbes de brillantor/contrast es "
"carregaran com a corbes d'Ajust del color."

#: ../../reference_manual/filters/adjust.rst:115
#: ../../reference_manual/filters/adjust.rst:118
msgid "Color Balance"
msgstr "Equilibri de color"

#: ../../reference_manual/filters/adjust.rst:120
msgid ""
"This filter allows you to control the color balance of the image by "
"adjusting the sliders for Shadows, Midtones and Highlights. The default "
"shortcut for this filter is :kbd:`Ctrl + B`."
msgstr ""
"Aquest filtre permet controlar l'equilibri de color de la imatge ajustant "
"els controls lliscants per a Ombres, Tons mitjans i Llums intenses. La "
"drecera predeterminada per a aquest filtre és :kbd:`Ctrl + B`."

#: ../../reference_manual/filters/adjust.rst:123
msgid ".. image:: images/filters/Color-balance.png"
msgstr ".. image:: images/filters/Color-balance.png"

#: ../../reference_manual/filters/adjust.rst:124
#: ../../reference_manual/filters/adjust.rst:162
msgid "Saturation"
msgstr "Saturació"

#: ../../reference_manual/filters/adjust.rst:124
msgid "Desaturation"
msgstr "Dessaturació"

#: ../../reference_manual/filters/adjust.rst:124
msgid "Gray"
msgstr "Gris"

#: ../../reference_manual/filters/adjust.rst:127
msgid "Desaturate"
msgstr "Dessatura"

#: ../../reference_manual/filters/adjust.rst:129
msgid ""
"Image-wide desaturation filter. Will make any image Grayscale. Has several "
"choices by which logic the colors are turned to gray. The default shortcut "
"for this filter is :kbd:`Ctrl + Shift + U`."
msgstr ""
"Filtre de dessaturació en tota la imatge. Crearà qualsevol imatge en escala "
"de grisos. Té diverses opcions segons les quals, la lògica farà que els "
"colors es tornin grisos. La drecera predeterminada per a aquest filtre és :"
"kbd:`Ctrl + Majús. + U`."

#: ../../reference_manual/filters/adjust.rst:133
msgid ".. image:: images/filters/Desaturate-filter.png"
msgstr ".. image:: images/filters/Desaturate-filter.png"

#: ../../reference_manual/filters/adjust.rst:134
#: ../../reference_manual/filters/adjust.rst:162
msgid "Lightness"
msgstr "Claredat"

#: ../../reference_manual/filters/adjust.rst:135
msgid "This will turn colors to gray using the HSL model."
msgstr "Canviarà els colors a gris utilitzant el model HSL."

#: ../../reference_manual/filters/adjust.rst:136
msgid "Luminosity (ITU-R BT.709)"
msgstr "Lluminositat (ITU-R BT.709)"

#: ../../reference_manual/filters/adjust.rst:137
msgid ""
"Will turn the color to gray by using the appropriate amount of weighting per "
"channel according to ITU-R BT.709."
msgstr ""
"Canviarà el color a gris utilitzant la quantitat apropiada de ponderació per "
"canal segons la recomanació ITU-R BT.709."

#: ../../reference_manual/filters/adjust.rst:138
msgid "Luminosity (ITU-R BT.601)"
msgstr "Lluminositat (ITU-R BT.601)"

#: ../../reference_manual/filters/adjust.rst:139
msgid ""
"Will turn the color to gray by using the appropriate amount of weighting per "
"channel according to ITU-R BT.601."
msgstr ""
"Canviarà el color a gris utilitzant la quantitat apropiada de ponderació per "
"canal segons la recomanació ITU-R BT.601."

#: ../../reference_manual/filters/adjust.rst:140
msgid "Average"
msgstr "Mitjana"

#: ../../reference_manual/filters/adjust.rst:141
msgid "Will make an average of all channels."
msgstr "Realitzarà una mitjana de tots els canals."

#: ../../reference_manual/filters/adjust.rst:142
msgid "Min"
msgstr "Mín."

#: ../../reference_manual/filters/adjust.rst:143
msgid "Subtracts all from one another to find the gray value."
msgstr ""
"Sostreu tots els canals l'un de l'altre per a trobar el valor del gris."

#: ../../reference_manual/filters/adjust.rst:145
msgid "Max"
msgstr "Màx."

#: ../../reference_manual/filters/adjust.rst:145
msgid "Adds all channels together to get a gray value."
msgstr "Afegeix tots els canals junts per obtenir un valor del gris."

#: ../../reference_manual/filters/adjust.rst:147
#: ../../reference_manual/filters/adjust.rst:150
msgid "Invert"
msgstr "Inverteix"

#: ../../reference_manual/filters/adjust.rst:147
msgid "Negative"
msgstr "Negatiu"

#: ../../reference_manual/filters/adjust.rst:152
msgid ""
"This filter like the name suggests inverts the color values in the image. So "
"white (1,1,1) becomes black (0,0,0), yellow (1,1,0) becomes blue (0,1,1), "
"etc. The default shortcut for this filter is :kbd:`Ctrl + I`."
msgstr ""
"Aquest filtre, com suggereix el nom, inverteix els valors de color a la "
"imatge. De manera que el blanc (1,1,1) es tornarà negre (0,0,0), el groc "
"(1,1,0) es tornarà blau (0,1,1), etc. La drecera predeterminada per a aquest "
"filtre és :kbd:`Ctrl + I`."

#: ../../reference_manual/filters/adjust.rst:155
msgid "Contrast"
msgstr "Contrast"

#: ../../reference_manual/filters/adjust.rst:158
msgid "Auto Contrast"
msgstr "Contrast automàtic"

#: ../../reference_manual/filters/adjust.rst:160
msgid "Tries to adjust the contrast the universally acceptable levels."
msgstr "Intenta ajustar el contrast dels nivells universalment acceptables."

#: ../../reference_manual/filters/adjust.rst:162
msgid "Hue"
msgstr "To"

#: ../../reference_manual/filters/adjust.rst:162
msgid "Value"
msgstr "Valor"

#: ../../reference_manual/filters/adjust.rst:162
msgid "Brightness"
msgstr "Brillantor"

#: ../../reference_manual/filters/adjust.rst:162
msgid "Chroma"
msgstr "Croma"

#: ../../reference_manual/filters/adjust.rst:165
msgid "HSV/HSL Adjustment"
msgstr "Ajust HSV/HSL"

#: ../../reference_manual/filters/adjust.rst:167
msgid ""
"With this filter, you can adjust the Hue, Saturation, Value or Lightness, "
"through sliders. The default shortcut for this filter is :kbd:`Ctrl + U`."
msgstr ""
"Amb aquest filtre, podreu ajustar el To, la Saturació, el Valor o la "
"Claredat mitjançant els controls lliscants. La drecera predeterminada per a "
"aquest filtre és :kbd:`Ctrl + U`."

#: ../../reference_manual/filters/adjust.rst:170
msgid ".. image:: images/filters/Hue-saturation-filter.png"
msgstr ".. image:: images/filters/Hue-saturation-filter.png"

#: ../../reference_manual/filters/adjust.rst:171
#: ../../reference_manual/filters/adjust.rst:174
msgid "Threshold"
msgstr "Llindar"

#: ../../reference_manual/filters/adjust.rst:171
msgid "Black and White"
msgstr "Blanc i negre"

#: ../../reference_manual/filters/adjust.rst:176
msgid ""
"A simple black and white threshold filter that uses sRGB luminosity. It'll "
"convert any image to a image with only black and white, with the input "
"number indicating the threshold value at which black becomes white."
msgstr ""
"Un filtre senzill de llindar del blanc i negre que utilitza la lluminositat "
"sRGB. Converteix qualsevol imatge a una imatge només en blanc i negre, amb "
"el número d'entrada que indicarà el valor del llindar en què el negre es "
"tornarà blanc."

#: ../../reference_manual/filters/adjust.rst:178
#: ../../reference_manual/filters/adjust.rst:191
msgid "Slope"
msgstr "Desnivell"

#: ../../reference_manual/filters/adjust.rst:178
msgid "ASC CDL"
msgstr "CDL de l'ASC"

#: ../../reference_manual/filters/adjust.rst:178
msgid "Offset and Power Curves"
msgstr "Corbes del desplaçament i potència"

#: ../../reference_manual/filters/adjust.rst:181
msgid "Slope, Offset, Power"
msgstr "Desnivell, desplaçament i potència"

#: ../../reference_manual/filters/adjust.rst:183
msgid ""
"A different kind of color balance filter, with three color selectors, which "
"will have the same shape as the one used in settings."
msgstr ""
"Un tipus diferent de filtre per a l'equilibri de color, amb tres selectors "
"de color, els quals tindran la mateixa forma que la utilitzada en els "
"ajustaments."

#: ../../reference_manual/filters/adjust.rst:185
msgid ""
"This filter is particular useful because it has been defined by the American "
"Society for Cinema as \"ASC_CDL\", meaning that it is a standard way of "
"describing a color balance method."
msgstr ""
"Aquest filtre és particularment útil perquè ha estat definit per l'American "
"Society for Cinema com a «ASC_CDL», el qual vol dir que és una forma "
"estàndard de descriure un mètode per a l'equilibri de color."

#: ../../reference_manual/filters/adjust.rst:192
msgid ""
"This represents a multiplication and determine the adjustment of the "
"brighter colors in an image."
msgstr ""
"Representa una multiplicació i determina l'ajust dels colors més brillants "
"en una imatge."

#: ../../reference_manual/filters/adjust.rst:193
msgid "Offset"
msgstr "Desplaçament"

#: ../../reference_manual/filters/adjust.rst:194
msgid ""
"This determines how much the bottom is offset from the top, and so "
"determines the color of the darkest colors."
msgstr ""
"Determina quant es desplaçarà la part inferior de la part superior, i així "
"determinarà el color dels colors més foscos."

#: ../../reference_manual/filters/adjust.rst:196
msgid ""
"This represents a power function, and determines the adjustment of the mid-"
"tone to dark colors of an image."
msgstr ""
"Representa una funció de potència i determinarà l'ajust del to mitjà als "
"colors foscos d'una imatge."
