# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:30+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Om KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr "Hjälpmenyn i Krita."

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr "Om"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr "Handbok"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr "Fel"

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr "Hjälpmenyn"

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Handbok Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr "Öppnar en webbläsare och skickar dig till den här handbokens index."

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Rapportera fel"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr "Skickar dig till felspåraren."

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr "Visar systeminformation för felrapporter."

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""
"Detta är ett urval av all teknisk information om datorn som är svår att "
"räkna ut. Det omfattar saker som vilken version av Krita som används, vilken "
"version operativsystemet har, och mest förtänksamt, vilken typ av OpenGL-"
"funktionalitet som datorn tillhandahåller. Det senaste varierar mycket "
"mellan datorer, och är på grund av det en av de svåraste att felsöka. Att "
"tillhandahålla sådan information kan hjälpa oss räkna ut vad som orsakar ett "
"fel."

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "Om Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr "Visar lista över medverkande."

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr "Berättar om KDE-gemenskapen som Krita ingår i."
