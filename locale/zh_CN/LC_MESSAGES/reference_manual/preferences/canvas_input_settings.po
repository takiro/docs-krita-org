msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___preferences___canvas_input_settings.pot\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "项目"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "画布输入设置"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
