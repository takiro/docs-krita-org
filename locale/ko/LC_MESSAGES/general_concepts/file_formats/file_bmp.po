# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-05-30 00:35+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../general_concepts/file_formats/file_bmp.rst:1
msgid "The Bitmap file format."
msgstr "비트맵 파일 형식입니다."

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "*.bmp"
msgstr "*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "BMP"
msgstr "BMP"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "Bitmap Fileformat"
msgstr "비트맵 파일 형식"

#: ../../general_concepts/file_formats/file_bmp.rst:15
msgid "\\*.bmp"
msgstr "\\*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:17
#, fuzzy
#| msgid ""
#| ".bmp, or Bitmap, is the simplest raster file format out there, and, being "
#| "patent-free, most programs can open and save bitmap files."
msgid ""
"``.bmp``, or Bitmap, is the simplest raster file format out there, and, "
"being patent-free, most programs can open and save bitmap files."
msgstr ""
".bmp, 비트맵 파일은 가장 간단한 래스터 파일 형식이며, 특허권으로 보호되지 않"
"기 때문에 많은 프로그램에서 다룰 수 있습니다."

#: ../../general_concepts/file_formats/file_bmp.rst:19
msgid ""
"However, most programs don't compress bitmap files, leading to BMP having a "
"reputation for being very heavy. If you need a lossless file format, we "
"actually recommend :ref:`file_png`."
msgstr ""
"그러나 많은 프로그램에서는 BMP 파일을 압축하지 않기 때문에 파일의 크기가 매"
"우 큽니다. 무손실 파일 형식으로 저장해야 한다면 :ref:`file_png` 형식을 추천합"
"니다."
